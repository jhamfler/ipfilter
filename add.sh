#!/bin/bash
curl https://lists.blocklist.de/lists/all.txt > blocklist
cat blocklist > alle.ips 2>/dev/null
echo "" >> alle.ips 2>/dev/null
cat mylist >> alle.ips 2>/dev/null
IPT=/sbin/iptables
SPAMLIST="spamlist"
SPAMDROPMSG="SPAM LIST DROP"
BADIPS=$(egrep -v -E "^#|^$" alle.ips)
# create a new iptables list
sudo $IPT -N $SPAMLIST
for ipblock in $BADIPS
do
   sudo $IPT -A $SPAMLIST -s $ipblock -j LOG --log-prefix "$SPAMDROPMSG"
   sudo $IPT -A $SPAMLIST -s $ipblock -j DROP
done
sudo $IPT -I INPUT -j $SPAMLIST
